<?php


class Lesson
{
    private $title;
    private $contr;
    private $chair;

    /**
     * Lesson constructor.
     * @param $title
     * @param $contr
     * @param $chair
     */
    public function __construct($title, $contr, $chair)
    {
        $this->title = $title;
        $this->contr = $contr;
        $this->chair = $chair;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getContr()
    {
        return $this->contr;
    }

    /**
     * @param mixed $contr
     */
    public function setContr($contr)
    {
        $this->contr = $contr;
    }

    /**
     * @return mixed
     */
    public function getChair()
    {
        return $this->chair;
    }

    /**
     * @param mixed $chair
     */
    public function setChair($chair)
    {
        $this->chair = $chair;
    }



}

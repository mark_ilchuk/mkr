<?php

include 'Lesson.php';

class Session
{
    private $lessons;

    public function __construct()
    {
        $this->lessons = array(
            new Lesson('Math', 'examination', 'TPZA'),
            new Lesson('History', 'test', 'TPZ'),
            new Lesson('Proga', 'examination', 'TPZA'),
            new Lesson('Eco', 'test', 'FIT'),
        );
    }

    public function getContr($contr)
    {
        $count = 0;
        foreach ($this->lessons as $lesson){
            if ($lesson->getContr()== $contr)
                $count++;
        }
        return $count;
    }

    public function getChair($chair)
    {
        $count = 0;
        foreach ($this->lessons as $lesson){
            if ($lesson->getChair()== $chair)
                $count++;
        }
        return $count;
    }
}

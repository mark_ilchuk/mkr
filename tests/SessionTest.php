<?php

include 'Session.php';

class SessionTest extends \PHPUnit\Framework\TestCase
{

    private $session;

    protected function setUp(): void
    {
        $this->session = new Session();
    }

    public function testContr()
    {
        $result = $this->session->getChair('FIT');
        $this->assertEquals(1, $result);
    }
    /**
     * @dataProvider addDataProvider
     * @param $chair
     */
    public function testChair($contr, $count)
    {
        $result = $this->session->getContr($contr);
        $this->assertEquals($count, $result);
    }

    public function addDataProvider(){
        return array(
            ['test', 2],
            ['examination', 3]
        );
    }

    protected function tearDown(): void
    {
        unset($this->session);
    }
}
